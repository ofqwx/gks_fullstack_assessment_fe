# README #

### GK Software fullstack assessment FrontEnd ###

## Pre Requirements

* Node JS v8.11.1
* Npm v5.6.0

## Getting Started

Install dependencies
```
cd gks_fullstack_assessment_fe
npm install
```

Run server
```
npm start
```

Then visit: http://localhost:3000

## Built With

* [React JS](http://reactjs.org)

### Contact ###

* Antonio Aznarez
* antonioaznarez@gmail.com