import _ from 'lodash';

/**
 * Class created as a helper to process data from complex json and create more readable object.
 */
class ShoppingCartTx {
  constructor (jsonTx) {
    this.qstr = 'com.gk_software.gkr.api.txpool.dto.';
    this.tx = jsonTx;
  }

  /** Transaction info */
  transaction () {
    return {
      id: this.tx.key.transactionID,
      type: this.tx.transactionTypeCode,
      total: _.get(this.tx, [
        'retailTransactionList', 0,
        `${this.qstr}RetailTransaction`,
        'retailTransactionTotalList', 0,
        `${this.qstr}RetailTransactionTotal`,
        'amount'
      ]),
      subTotal: _.get(this.tx, [
        'retailTransactionList', 0,
        `${this.qstr}RetailTransaction`,
        'retailTransactionTotalList', 1,
        `${this.qstr}RetailTransactionTotal`,
        'amount'
      ]),
      subTotalDiscount: _.get(this.tx, [
        'retailTransactionList', 0,
        `${this.qstr}RetailTransaction`,
        'retailTransactionTotalList', 2,
        `${this.qstr}RetailTransactionTotal`,
        'amount'
      ]),
      currency: this.tx.isocurrencyCode === 'EUR' ? '€' : this.tx.isocurrencyCode,
      endDate: new Date(this.tx.endDateTimestamp)
    };
  }

  /** Sale Return info */
  saleReturnItems () {
    const srItems = _.filter(_.get(this.tx, [
      'retailTransactionList', 0,
      `${this.qstr}RetailTransaction`,
      'retailTransactionLineItemList'
    ]).map((o) => { // Map to get first element of object
      return o[Object.keys(o)[0]];
    }), (i) => {
      return i.retailTransactionLineItemTypeCode === 'SR';
    }).map((rtItem) => { // Map into sale return line items to get data
      const srItem = rtItem.saleReturnLineItemList[0][`${this.qstr}SaleReturnLineItem`];
      // Return processed data ready to use directly in component
      return {
        description: srItem.receiptText,
        units: srItem.units,
        actualUnitPrice: srItem.actualUnitPrice,
        extendedAmount: srItem.extendedAmount,
        grandExtendedAmount: srItem.grandExtendedAmount,
        extendedDiscountAmount: srItem.extendedDiscountAmount,
        promotion: this.promotion(srItem)
      };
    });
    return srItems;
  }

  /** Price Modification Line items info */
  priceModItems () {
    const pmItems = _.filter(_.get(this.tx, [
      'retailTransactionList', 0,
      `${this.qstr}RetailTransaction`,
      'retailTransactionLineItemList'
    ]).map((o) => { // Map to get first element of object
      return o[Object.keys(o)[0]];
    }), (i) => {
      return i.retailTransactionLineItemTypeCode === 'PM';
    }).map((rtItem) => { // Map into sale return line items to get data
      const pmItem = rtItem.priceModificationLineItemList[0][`${this.qstr}PriceModificationLineItem`];
      // Return processed data ready to use directly in component
      return {
        percentage: pmItem.percentage,
        amount: pmItem.amount
      };
    });
    return pmItems;
  }

  /** Promotion info */
  promotion (srItem) {
    const promDetail = {};
    // If discount field is under 0 then we have promotion/s
    if (srItem.extendedDiscountAmount < 0) {
      promDetail.hasPromotion = true;
      promDetail.total = srItem.extendedDiscountAmount;
      promDetail.promotions = srItem.retailPriceModifierList.map((rpm) => {
        return {
          amount: rpm[Object.keys(rpm)[0]].amount,
          percent: rpm[Object.keys(rpm)[0]].percent
        };
      });
      return promDetail;
    } else { // If not hasPromotion it's set to false
      promDetail.hasPromotion = false;
      return promDetail;
    }
  }
};

export default ShoppingCartTx;
