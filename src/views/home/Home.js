import React, { Component } from 'react';
import TransactionsList from '../../components/TransactionsList';
import './Home.css';

/**
 * Home component used as a start view of the app wich render TransactionList component.
 */
class Home extends Component {
  render () {
    return (
      <div className='Home'>
        <header className='Home-header'>
          <h1 className='Home-title'>Purchases</h1>
        </header>
        <TransactionsList />
      </div>
    );
  }
}

export default Home;
