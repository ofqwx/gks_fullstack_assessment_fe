import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome';
import ShoppingCartTx from '../../utils/shoppingCartClass';
import TransactionDetail from './TransactionDetail';
import './transaction.css';

/**
 * Transaction component with quick view info.
 */
class Transaction extends Component {
  constructor (props) {
    super(props);
    this.updateTotals = this.updateTotals.bind(this);
    /** Create a shopping cart object which handle complex json data for us. */
    const shoppingCartTx = new ShoppingCartTx(props.tx);
    this.state = {
      isHidden: true,
      editing: false,
      tx: shoppingCartTx.transaction(),
      srItems: shoppingCartTx.saleReturnItems(),
      pmItems: shoppingCartTx.priceModItems()
    };
  };

  /** Method to update Transaction info in this components when units in detail change. */
  updateTotals (tx) {
    this.setState({tx});
  }

  /** Method to show or hide TransactionDetail component */
  toggleHidden () {
    this.setState({
      isHidden: !this.state.isHidden
    });
  };

  /** Method to switch TransactionDetail component from read to edit state. */
  toggleEditing () {
    this.setState({
      editing: !this.state.editing
    });
  };

  render () {
    return (
      <div className='flexContainer striped'>
        <div className='flexItem leftCol' >
          <h3>{`${this.state.tx.endDate.getFullYear()}-${this.state.tx.endDate.getMonth()}-${this.state.tx.endDate.getDate()}`}</h3>
          <h3>
            Items: {this.state.srItems.length}
            <a className='btnDefault' onClick={this.toggleHidden.bind(this)}>{this.state.isHidden ? ' details' : ' hide details'}</a>
          </h3>
        </div>
        <div className='flexItem rightCol'>
          <p>Ref. Code: <strong>{this.state.tx.id.substr(this.state.tx.id.length - 5)}</strong></p>
          {!this.state.isHidden && <FontAwesome className='btnDefault' name='edit' size='2x' onClick={this.toggleEditing.bind(this)} />}
          {this.state.isHidden && <h2>Total: {this.state.tx.currency} {this.state.tx.total}</h2>}
        </div>
        <TransactionDetail
          hide={this.state.isHidden}
          updateTotals={this.updateTotals}
          tx={this.state.tx}
          srItems={this.state.srItems}
          pmItems={this.state.pmItems}
          editing={this.state.editing} />
      </div>
    );
  }
};

export default Transaction;
