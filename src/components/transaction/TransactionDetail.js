import React, { Component } from 'react';
import _ from 'lodash';

/**
 * TransactionDetail component with details of each RetailTransactionLineItem.
 */
class TransactionDetail extends Component {
  constructor (props) {
    super(props);
    this.state = {
      tx: props.tx,
      srItems: props.srItems,
      pmItems: props.pmItems
    };
  }

  /** Update totals of transaction after component mount */
  componentDidMount () {
    this.updateTotals();
  };

  /** Method to change item units and set this change in the component state. */
  changeUnits (index, event) {
    let srItems = _.clone(this.state.srItems, true);
    srItems[index].units = event.target.value;
    srItems[index].extendedAmount = srItems[index].units * srItems[index].actualUnitPrice;
    if (srItems[index].promotion.hasPromotion) {
      srItems[index].extendedDiscountAmount = this.promotionCalc(srItems[index]);
    }
    srItems[index].grandExtendedAmount = (srItems[index].extendedAmount - srItems[index].extendedDiscountAmount).toFixed(2);
    this.setState({srItems});
    this.updateTotals();
  };

  /** Method to update totals from LineItems data */
  updateTotals () {
    /** Since objects properties can't be edited inside of a component state,
     * we clone the object and then we set state with the edited one.
      */
    let tx = {...this.state.tx};
    let subTotal = 0;
    let subTotalDiscount = 0;
    this.state.srItems.forEach((item) => {
      subTotal += item.extendedAmount;
      subTotalDiscount += Math.abs(item.extendedDiscountAmount);
    });
    /** Check if there is any price modification */
    if (this.state.pmItems.length > 0) {
      this.state.pmItems.forEach((pm) => {
        subTotalDiscount += Math.abs(subTotal * pm.percentage / 100);
      });
    }
    tx.subTotal = subTotal.toFixed(2);
    tx.subTotalDiscount = subTotalDiscount.toFixed(2);
    tx.total = (subTotal - subTotalDiscount).toFixed(2);
    this.setState({tx});
    /** Update totals in parent component */
    this.props.updateTotals(tx);
  };

  /** Method to calculate promotion amount of each LineItem if they have any */
  promotionCalc (item) {
    let itemsTotal = item.extendedAmount;
    let promotionAmount = 0;
    item.promotion.promotions.forEach((p) => {
      if (p.percent > 0) {
        const percentValue = itemsTotal * p.percent / 100;
        promotionAmount += percentValue;
        itemsTotal -= percentValue;
      } else {
        promotionAmount += Math.abs(p.amount);
        itemsTotal -= Math.abs(p.amount);
      }
    });
    return promotionAmount.toFixed(2);
  };

  render () {
    return (
      <div className={`flexContainer detail ${this.props.hide ? 'hide' : ''}`}>
        {/** List of Sale Retail Items */}
        <div className='flexItem leftCol detail'>
          {this.state.srItems.map((d, index) => {
            if (this.props.editing) {
              return (
                <p key={index}>
                  {`${d.description} (${this.props.tx.currency} ${d.actualUnitPrice}) x `}
                  {<input type='number' size='3' defaultValue={d.units} onChange={(e) => this.changeUnits(index, e)} className='unitsInput' />}
                  {d.promotion.hasPromotion ? ` - Promotion ${this.props.tx.currency} ${Math.abs(d.extendedDiscountAmount)}: ` : ''}
                  {<strong>{this.state.tx.currency} {d.grandExtendedAmount}</strong>}
                </p>
              );
            } else {
              return (
                <p key={index}>
                  {`${d.description} (${this.props.tx.currency} ${d.actualUnitPrice})`}
                  {` x ${d.units} `}
                  {d.promotion.hasPromotion ? ` - Prom. of ${this.props.tx.currency} ${Math.abs(d.extendedDiscountAmount)}: ` : ''}
                  {<strong>{this.state.tx.currency} {d.grandExtendedAmount} </strong>}
                </p>
              );
            }
          })}
          {/** List of Price Modification Items */}
          {this.props.pmItems.map((p, index) => {
            return <p key={index}> {`Promotion ${p.percentage ? `${p.percentage}%` : ''} ${this.props.tx.currency} ${p.amount}`}</p>;
          })}
        </div>
        <div className='flexItem rightCol detail'>
          {<h3>Total: {this.state.tx.currency} {this.state.tx.total}</h3>}
          {<h3>Discount: {this.state.tx.currency} {this.state.tx.subTotalDiscount}</h3>}
          {<h3>Subtotal: {this.state.tx.currency} {this.state.tx.subTotal}</h3>}
        </div>
      </div>
    );
  }
};

export default TransactionDetail;
