import React, { Component } from 'react';
import Transaction from './transaction/Transaction';
import FontAwesome from 'react-fontawesome';

/**
 * TransactionList component which call backend api to retrieve data and render Transaction component.
 */
class TransactionsList extends Component {
  constructor () {
    super();
    this.state = {
      load: false
    };
  }

  /** After component is mounted we fetch data from backend api. */
  componentDidMount () {
    /** First we get transactions ids */
    return fetch('http://localhost:3001/tx')
      .then(response => response.json())
      .then((txIds) => {
        /** Then we have all ids we wait for get all transactions. */
        Promise.all(
          txIds.map(txId => fetch(`http://localhost:3001/tx/${txId}`)
            .then(tx => tx.json())
          )
        )
          /** Finally we set load to true and transactions data in the component state. */
          .then(txs => {
            this.setState({
              load: true,
              txs
            });
          });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  render () {
    if (!this.state.load) {
      return (
        <FontAwesome
          name='cog'
          size='5x'
          spin
          style={{position: 'absolute', top: '50%'}}
        />
        // <h1>Loading Data</h1>
      );
    }
    return (
      <div>
        {this.state.txs.map((tx, index) => {
          if (tx.transactionTypeCode === 'RTLTRN') {
            return <Transaction key={index} tx={tx} />;
          }
        })}
      </div>
    );
  }
};

export default TransactionsList;
